# Utopia Documentation Server

Implements a docker- and python-based web server to host the Utopia documentation.

This repository holds the configuration required for this to work, but assumes that it is run on the group's `hermes` server, which provides the remaining infrastructure (docker + traefik).

## How to deploy the server

In your current terminal session, define the environment variable
`DOC_SERVER_NAME` to specify the name of the directory which will contain the
documentation. **Do not** use slahes (`/`) in this variable! This name will
also be used for the name of the resulting Docker container. It is recommended
to append it with `_doc`, like `my_project_doc`. Then, start the container.

```bash
export DOC_SERVER_NAME=utopia_doc
docker-compose up
```

### Notes regarding keys and authentication
* Populate `authorized_keys` with the public keys that are allowed to access the container
* Generate a set of host keys in the `host_keys` directory (scheme: `ssh_host_<algo>_key.[pub]`)
    * Necessary algorithms are `ed25519` and `ecdsa`
    * Can use `ssh-keygen -t ecdsa -b 521 -f path/to/file` and `ssh-keygen -t ed25519 -f /path/to/file` for that
* Inspect the host key both via `ssh-keyscan -H -p <port> <ip>` and `... <domain>` and add the entries to `known_hosts` file in the container that is to access the `doc_server` container.
