#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Requiring 3 positional arguments: [directory to serve, abs. path] "\
         "[server script, abs.path] [server port], but got $#!"
    exit 1
fi

# Gather the variables
SERVE_DIR=$1
SERVE_SCRIPT=$2
SERVE_PORT=$3

echo "Hello Utopist!"
echo "It's now $(date)"
echo ""

# -- Need to install some software --------------------------------------------
echo "Installing required software ..."

# Update package manager
# NOTE This is meant to run on debian linux
apt-get update

# openssh
apt-get install -y openssh-server

# cleanup
apt-get clean
echo "Software installation finished."
echo ""

# -- SSH setup ----------------------------------------------------------------
echo "Setting up sshd ..."

# Copy over the passed sshd_config, overwriting the existing one
echo "Installing custom sshd_config ..."
cp -v /tmp/sshd_config /etc/ssh/sshd_config

# Set host keys generated outside
echo "Installing specified host keys ..."
cp -rv /tmp/ssh_host_keys/* /etc/ssh/

# Set appropriate ownership and permissions for authorized_keys file
echo "Setting permissions ..."
chown -R root /root/.ssh
chmod 700 /root/.ssh
chmod 600 /root/.ssh/authorized_keys

# Now start the service
/etc/init.d/ssh start
/etc/init.d/ssh status

# Create unique moduli files, if they are older than 6 months (i.e. probably not unique)
if test `find "/etc/ssh/moduli" -mmin +259200`
then
    echo "Generating new moduli files ..."
    ssh-keygen -G moduli-2048.candidates -b 2048
    ssh-keygen -T moduli-2048 -f moduli-2048.candidates
    cp moduli-2048 /etc/ssh/moduli
    rm moduli-2048
fi

echo "Setup of sshd finished."
echo ""

# -- Start the web server -----------------------------------------------------
# Move to the directory that should be served as static web pages
echo "Changing directory to serving directory ..."
cd $SERVE_DIR
echo "Now in: `pwd`"

# Start the server script, passing the port
echo "Invoking $SERVE_SCRIPT python script to run server on port $SERVE_PORT ..."
python $home/$SERVE_USER/$SERVE_SCRIPT $SERVE_PORT

# Done.
echo ""
echo ""
echo "Entrypoint script ended at $(date)."
echo ""

exit 0
