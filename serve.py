"""Runs a (very) simple web server, handling basic HTTP requests and providing
access to the files in the current working directory.
"""

import sys
from http.server import HTTPServer, SimpleHTTPRequestHandler

class DocServer(HTTPServer):
    """Specialization of the HTTP server for the purpose of serving a static
    documentation
    """
    def serve_forever(self, *args, **kwargs):
        """Run the web server until encountering KeyboardInterrupt"""
        try:
            super().serve_forever(*args, **kwargs)
        
        except KeyboardInterrupt:
            pass

        finally:
            self.server_close()

# -----------------------------------------------------------------------------
if __name__ == '__main__':
    # Get the port
    port = int(sys.argv[1])

    # Setup and run the server
    httpd = DocServer(('', port), SimpleHTTPRequestHandler)

    print("Now running web server on port {} ... will serve forever."
          "".format(port))
    httpd.serve_forever()

    print("\nWeb server shut down.")
