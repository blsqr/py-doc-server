#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Requiring 3 positional arguments: [doc directory, abs. path] "\
         "[latest dir] [num youngest documentations to keep], but got $#!"
    exit 1
fi

# Get arguments
DOC_DIR=$1
LATEST=$2
KEEP_LAST_N=$3

echo "Postprocessing documentation directory $DOC_DIR ..."

cd $DOC_DIR 
echo "Current content, sorted by creation date:"
find $(pwd)/* -maxdepth 0 -printf '  %T+  %p\n' | sort 

# -- Link --------------------------------------------------------------------
echo "Linking latest documentation ..."

# ... but only if it is valid
if [ ! -d "$LATEST" ]; then
    echo "Missing directory $LATEST! Will not link."
    exit 1
fi

rm latest
ln -s $LATEST latest
echo "Latest documentation now points to: $LATEST"
echo ""

# -- Clean up ----------------------------------------------------------------
echo "Cleaning up $DOC_DIR by deleting all but the youngest $KEEP_LAST_N directories ..."
find $DOC_DIR/* -maxdepth 0 -type d -printf '%T+ %p\n' | sort | head -n -$KEEP_LAST_N | xargs rm -rfv
echo ""

echo "Postprocessing successfully finished."
exit 0
